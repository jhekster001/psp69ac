<?php
   	include('session.php');
	include('./dbcon.php');

	if(isset($_POST['submit'])){
		$anatomic_fullname = addslashes($_POST['fullname']);
		$anatomic_position = addslashes($_POST['position']);
		$anatomic_title = addslashes($_POST['title']);
		$anatomicl_subtitle = addslashes($_POST['subtitle']);
		$anatomic_message = addslashes($_POST['message']);		

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		$sql = "INSERT INTO anatomic SET 
				fullname='".$anatomic_fullname."', 
				position='".$anatomic_position."',
				title='".$anatomic_title."', 
				subtitle='".$anatomic_subtitle."', 
				message='".$anatomic_message."', 
				file_path_dp=''";

		if ($conn->query($sql) === TRUE) {
		    $last_id = $conn->insert_id;
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		//Upload
		$path = 'uploads/anatomic/';
		if(!file_exists($path)){
			mkdir('uploads/anatomic/', 0777, true);
		}
		$anatomic_ext = pathinfo($_FILES["anatomic_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["anatomic_dp"]["name"] != ''){
			$anatomic_target_file = $path . 'anatomic-dp_'.$last_id.'.'.$anatomic_ext;
			move_uploaded_file($_FILES["anatomic_dp"]["tmp_name"], $anatomic_target_file);		
		}else{
			$anatomic_target_file = '';
		}		

		$update_sql = "UPDATE anatomic SET 
						file_path_dp='".$anatomic_target_file."' 
						WHERE id='".$last_id."'";

		if ($conn->query($update_sql) === TRUE) {
		    header("Location: anatomic-pathology-speaker-admin.php");
		} else {
		    echo "Error: " . $update_sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 
