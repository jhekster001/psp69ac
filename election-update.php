<?php
   include('session.php');
include('./dbcon.php');

	if(isset($_POST['submit'])){
	//President
		$elec_fullname = addslashes($_POST['fullname']);
		$elec_position = addslashes($_POST['position']);
		$elec_title = addslashes($_POST['title']);
		$elec_subtitle = addslashes($_POST['subtitle']);
		$elec_message = addslashes($_POST['message']);
		$elec_title_2 = addslashes($_POST['title2']);
		$elec_subtitle_2 = addslashes($_POST['subtitle2']);
		$elec_message_2 = addslashes($_POST['message2']);

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		//Upload
		$path = 'uploads/election/';
		if(!file_exists($path)){
			mkdir('uploads/election/', 0777, true);
		}
		$election_dp_ext = pathinfo($_FILES["election_dp"]["name"], PATHINFO_EXTENSION);		
		$election_img_ext = pathinfo($_FILES["election_img"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["election_dp"]["name"] != ''){
			$election_dp_target_file = $path . 'election_dp'.'.'.$election_dp_ext;
			move_uploaded_file($_FILES["election_dp"]["tmp_name"], $election_dp_target_file);		
		}else{
			$election_select_sql = 'SELECT * from election';
			$result_election = $conn->query($election_select_sql);
			$elec_details = $result_election->fetch_assoc();
			$election_dp_target_file = $elec_details['file_path_dp'];
		}


		if($_FILES["election_img"]["name"] != ''){
			$election_img_target_file = $path . 'election_img'.'.'.$election_img_ext;
			move_uploaded_file($_FILES["election_img"]["tmp_name"], $election_img_target_file);		
		}else{
			$election_select_sql = 'SELECT * from election';
			$result_election = $conn->query($election_select_sql);
			$elec_details = $result_election->fetch_assoc();
			$election_img_target_file = $elec_details['file_path_img'];
		}		


		$sql = "UPDATE election SET
				fullname='".$elec_fullname."', 
				position='".$elec_position."',
				title='".$elec_title."', 
				subtitle='".$elec_subtitle."', 
				message='".$elec_message."',
				title_2='".$elec_title_2."',
				subtitle_2='".$elec_subtitle_2."',
				message_2='".$elec_message_2."',
				file_path_dp='".$election_dp_target_file."',
				file_path_img='".$election_img_target_file."'";

		if ($conn->query($sql) === TRUE) {
			header("Location: election-for-board-of-governors-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 