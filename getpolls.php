<?php 
	include('./dbcon.php');
	$mysqli = new mysqli($servername, $username, $password, $dbname);

	/* check connection */
	if ($mysqli->connect_errno) {
	    printf("Connect failed: %s\n", $mysqli->connect_error);
	    exit();
	}
	// $sql = "SELECT * FROM poll";
	$sql = "SELECT poll.value, poll.text, poll.fontawesome, COUNT(attendees.value) as count
			FROM poll
			LEFT JOIN attendees ON poll.value = attendees.value
			GROUP BY poll.value
			ORDER BY poll.id ASC";
	$result = $mysqli->query($sql);
	$myObj = [];
	while ($row = $result->fetch_assoc()) {		
		$myObj[] = $row;
	}						      

	echo json_encode( $myObj );
 ?>