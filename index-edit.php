<?php
   include('session.php');
?>
<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">
    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/psp.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <title>PSP Admin</title>
  </head>
<body>

	<?php include('./components-admin/header-admin.php') ?>
		<div class="jumbotron bg-light mb-0 pb-0 mt-3">
			<div class="row">
				<div class="col-lg-2 d-none d-sm-block">					
					<?php include('./components-admin/sidebar-admin.php') ?>	
				</div>

					<!-- ************//PAGE//************ -->
					<div class="content col-lg-8 col-sm-12 mt-3 pt-3" id="home-content">
						<img src="img/banner.jpg" id="banner" style="border-style: solid; border-color: grey;">
						
						<p></p>
						<h4><b>PHILIPPINE SOCIETY OF PATHOLOGIST. INC - 1950</b></h4>
						<p>The organizational meeting was held at the Aristocrat Restaurant on Roxas Boulevard on the night of April 11, 1950. The first set of officers was elected namely: President, Dr. Liborio Gomez; Vice-President, Dr. Juan Z. Sta. Cruz; Secreatry-Treasurer, Dr. Benjamin A, Barrera; Governors: Drs. Onofre Garcia, Walfredo De Leon, Manuel D. Penas, and Benjamin A. Barrera.</p>

						<p>The PSP Board of Pathology was created in the year 1966-1967 to administer the examinations and grants certificates of qualification for the practice of pathology after satisfactory demonstration as Regular Member and eventual evaluation of Fellow in the Society.</p>

						<p>On June 18, 1966 the Clinical Laboratory Law (RA 4688) was passed by the 6th Congress of the Philippines with valuable contributions extended by the Society in defining the role of the profession and scope of practice of pathology in the country. Subsequently the implementing rules governing the establishment, registration, operation and maintenance of clinical laboratories were approved and signed by then Secretary of Health Paulino J. Garcia. The Society was also admitted as a specialty affiliate of the Philippine Medical Association.</p>

						<p>In the following years, the need for improving quality patient care with assurance of quality laboratory output became the specific thrust of the Society with the cooperation of the Bureau of Research and Laboratories.</p>

						<p>The Society undertook series of seminars, workshops and symposia in collaboration with some national specialty, international organizations such as the World Health Organization and government agencies particularly the Ministry of Health. These activities are now the mainstay of the current thrust to upgrade both manpower and institutional developments of the country.</p>

						<p>The ground breaking and cornerstone laying on the side of the future PSP builing was held on February 20, 1986. The PSP also became member of the Asia Pacific Association of Society of Pathologists.</p>

						<p>The turn of the century was characterized by rapid advances in Medicine, computer technology and communication. The new millennium poses new challenges. As the Society celebrates its 50th year of existence, its commitment to excellence in health service, training and research remains unabated bringing the practice of pathology to a renewed and grater height. It could be stated that the practice of pathology in the Philippines is at par with those in other countries and the Filipino pathologists is an important partner of other doctors in the management of patients.
						</p>
						<br>
					</div>
			</div>
		</div>
					<div class="p-5">
						<?php include('./components-admin/footer-admin.php') ?>
					</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <script type="text/javascript" src="./js/script-admin.js"></script>
  </body>
</html>