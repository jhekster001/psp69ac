<?php
   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
	//President
		$opening_fullname = addslashes($_POST['opening_fullname']);
		$opening_position = addslashes($_POST['opening_position']);
		$opening_title = addslashes($_POST['opening_title']);
		$opening_subtitle = addslashes($_POST['opening_subtitle']);
		$opening_message = addslashes($_POST['opening_message']);

		//Chairperson
		$closing_fullname = addslashes($_POST['closing_fullname']);
		$closing_position = addslashes($_POST['closing_position']);
		$closing_title = addslashes($_POST['closing_title']);
		$closing_subtitle = addslashes($_POST['closing_subtitle']);
		$closing_message = addslashes($_POST['closing_message']);


		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		$path = 'uploads/keynote/';
		if(!file_exists($path)){
			mkdir('uploads/keynote/', 0777, true);
		}
		$opening_ext = pathinfo($_FILES["opening_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["opening_dp"]["name"] != ''){
			$opening_target_file = $path . 'opening-speaker'.'.'.$opening_ext;
			move_uploaded_file($_FILES["opening_dp"]["tmp_name"], $opening_target_file);		
		}else{
			$opening_select_sql = 'SELECT * from keynote_speaker WHERE type ="opening"';
			$result_opening = $conn->query($opening_select_sql);
			$opening_details = $result_opening->fetch_assoc();
			$opening_target_file = $opening_details['file_path_dp'];		
		}

		$opening_sql = "UPDATE keynote_speaker SET 
				fullname='".$opening_fullname."', 
				position='".$opening_position."', 
				title='".$opening_title."', 
				subtitle='".$opening_subtitle."', 
				message='".$opening_message."', 
				file_path_dp='".$opening_target_file."' 
			WHERE 
				type='opening'";

		$closing_ext = pathinfo($_FILES["closing_dp"]["name"], PATHINFO_EXTENSION);		
		if($_FILES["closing_dp"]["name"] != ''){
			$closing_target_file = $path . 'closing-speaker'.'.'.$closing_ext;
			move_uploaded_file($_FILES["closing_dp"]["tmp_name"], $closing_target_file);	
		}else{
			$closing_select_sql = 'SELECT * from keynote_speaker WHERE type ="closing"';
			$result_closing = $conn->query($closing_select_sql);
			$closing_details = $result_closing->fetch_assoc();
			$closing_target_file = $closing_details['file_path_dp'];

		}

		$closing_sql = "UPDATE keynote_speaker SET 
				fullname='".$closing_fullname."', 
				position='".$closing_position."', 
				title='".$closing_title."', 
				subtitle='".$closing_subtitle."', 
				message='".$closing_message."', 
				file_path_dp='".$closing_target_file."' 
			WHERE 
				type='closing'";		

		if ($conn->query($opening_sql) === TRUE && $conn->query($closing_sql)) {
			header("Location: keynote-speaker-admin.php");
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}
	
 ?>

 