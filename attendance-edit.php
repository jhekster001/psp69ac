<?php
   include('session.php');
?>

<?php
include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM attendance";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
		$attendance_details = $result->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/psp.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <!-- RTE -->
    <link rel="stylesheet" type="text/css" href="./css/richtext.min.css">

    <title>PSP Admin</title>
  </head>
<body>

	<?php include('./components-admin/header-admin.php') ?>
		<div class="jumbotron bg-light mb-0 pb-0 mt-3">
			<div class="row">
				<div class="col-lg-2 d-none d-sm-block">					
					<?php include('./components-admin/sidebar-admin.php') ?>	
				</div>

				<!-- ************//CONTENT//************ -->
				<div class="content col-lg-8 col-sm-12 mt-3 pt-3" id="home-content">
					<img src="img/attendance.jpg" id="banner" style="border-style: solid; border-color: grey;">
					<h4 class="title mt-3"><b><i class="fas fa-edit"></i> EDIT ATTENDANCE</b></h4>
					<hr style="width: 100%; opacity: 0.2;">
					<br>
					<form method="post" enctype="multipart/form-data" action="attendance-update.php">
						<div class="container">
							<div class="row">
								<div class="col-12">		
									<div class="form-group" style="border: solid grey;">
									<div class="card" style="background-color:#edd8c1;">
									    <br>
										<div class="card-body">
											<label><b>Title:</b></label>
											<p><input type="text" value="<?php echo $attendance_details['title'];?>" name="title" class="form-control"></p>
											<label><b>Subtitle:</b></label>
											<p><input type="text" value="<?php echo $attendance_details['subtitle'];?>" name="subtitle" class="form-control"></p>
										    <label for="exampleInputFile"><h4><b>Update Display Picture</b></h4>
								    		</label>
										    <input type="file" name="attendance_dp" class="form-control-file mb-5" id="exampleInputFile" aria-describedby="fileHelp">
										</div>
									</div>
									</div>
								</div> 								
							</div>		
							<hr><br>
							<div class="row d-flex justify-content-end pt-3">
								<button type="submit" name="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>&nbsp
								<a href="attendance-admin.php" class="btn btn-secondary"><i class="fa fa-times"></i> Cancel</a>								
							</div>
						</div>
						<br>
					</form>
				</div>
			</div>
		</div>
		<div class="p-5">
			<?php include('./components-admin/footer-admin.php') ?>
		</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./js/jquery.richtext.min.js"></script>
    <script type="text/javascript">
    	$(function(){
    		$('.message').richText();
    	})
    </script>

  </body>
</html>