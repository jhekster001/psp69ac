<div class="btn-group-vertical" id="navbtn">
	<a href="index.php" class="btn btn-dark navbtn" id="home">Home</a>

	<a href="welcome-message.php" class="btn btn-dark navbtn" id="welcome-message">Welcome Message</a>

	<a href="about-the-convention.php" class="btn btn-dark navbtn" id="about-convention">About the Convention</a>

	<a href="convention-venue.php" class="btn btn-dark navbtn" id="convention-venue">Convention Venue</a>

	<a href="announcements.php" class="btn btn-dark navbtn" id="announcements">Announcements</a>

	<a href="convention-house-rules.php" class="btn btn-dark navbtn" id="house-rules">Convention House Rules</a>

	<a href="attendance.php" class="btn btn-dark navbtn" id="attendance">Attendance</a>

	<a href="organizing-committee.php" class="btn btn-dark navbtn" id="organizing-committee">Organizing Committee</a>

	<a href="keynote-speaker.php" class="btn btn-dark navbtn" id="keynote-speaker">Keynote Speaker</a>

	<a href="anatomic-pathology-speaker.php" class="btn btn-dark navbtn" id="anatomic-speaker">Anatomic Pathology Speakers</a>

	<a href="clinical-pathology-speakers.php" class="btn btn-dark navbtn" id="clinical-speaker">Clinical Pathology Speakers</a>

	<a href="awards.php" class="btn btn-dark navbtn" id="awards">Awards</a>

	<a href="program.php" class="btn btn-dark navbtn" id="program">Program</a>

	<a href="registration.php" class="btn btn-dark navbtn" id="registration">Registration</a>

	<a href="election-for-board-of-governors.php" class="btn btn-dark navbtn" id="election-governors">Election for Board of Governors</a>

	<a href="congress-sponsors.php" class="btn btn-dark navbtn" id="congress-sponsors">Congress Sponsors</a>

	<a href="the-psp-secretariat.php" class="btn btn-dark navbtn" id="psp-secretariat">The PSP Secretariat</a>

	<a href="contact-the-psp-secretariat.php" class="btn btn-dark navbtn" id="contact-psp">Contact the PSP Secretariat</a>

	<!-- 	<a href="about.php" class="btn btn-dark navbtn" id="about">About</a> -->
</div>	