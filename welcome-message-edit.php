<?php
   include('session.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/psp.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <!-- RTE -->
    <link rel="stylesheet" type="text/css" href="./css/richtext.min.css">

    <title>PSP Admin</title>
  </head>
<body>


	<?php include('./components-admin/header-admin.php') ?>
		<div class="jumbotron bg-light mb-0 pb-0 mt-3">
			<div class="row">
				<div class="col-lg-2 d-none d-sm-block">					
					<?php include('./components-admin/sidebar-admin.php') ?>	
				</div>
				
<?php
include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$pres_sql = "SELECT * FROM welcome_message WHERE type='president'";
$chair_sql = "SELECT * FROM welcome_message WHERE type='chair'";
$result_pres = $conn->query($pres_sql);
$result_chair = $conn->query($chair_sql);

if ($result_pres->num_rows > 0) {
		$president_details = $result_pres->fetch_assoc();
} else {
    echo "0 results";
}
if ($result_chair->num_rows > 0) {
		$chair_details = $result_chair->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();
?>				

			<!-- ************//CONTENT/************ -->
			<div class="content col-lg-8 col-sm-12 mt-3 pt-3" id="home-content">
				<img src="img/welcome.jpg" id="banner" style="border-style: solid; border-color: grey;">
				<h4 class="title mt-3"><b> WELCOME MESSAGE DETAILS</b></h4>
				<hr style="width: 100%; opacity: 0.2;">
				<br>
				<form method="post" enctype="multipart/form-data" action="welcome-message-update.php">
					<div class="container">
						<h4 class="title mt-3"><b><i class="fas fa-edit"></i> EDIT PSP PRESIDENT DETAILS</b></h4><br>
						<div class="row">
							<div class="col-4">		
								<div class="form-group" style="border: solid grey;">
								<div class="card" style="background-color:#edd8c1;">
								    <br>
								    <label for="exampleInputFile" style="margin-left: 20px;"><h4><b>Update Display Picture</b></h4>
									<p style="color: green;"><b>Note: Photo must be in "3 x 4" inches size (recommended)</b></p>
								    </label>
								    <input type="file" name="president_dp" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" style="margin-left: 20px;">
								</div>
									<div class="card-body">
										<label><b>Fullname:</b></label>
										<p><input type="text" value="<?php echo $president_details['fullname'];?>" name="president_fullname" class="form-control"></p>
										<label><b>Position:</b></label>
										<p><input type="text" value="<?php echo $president_details['position'];?>" name="president_position" class="form-control"></p>
									</div>
								</div>
							</div> 
							<div class="col-8" style="border: solid grey;"><br>							
								<label><b>Title:</b></label>
								<p><input type="text" value="<?php echo $president_details['title'];?>" name="president_title" class="form-control"></p>
								<label><b>Subtitle:</b></label>
								<p><input type="text" value="<?php echo $president_details['subtitle'];?>" name="president_subtitle" class="form-control"></p>
								<label><b>Message:</b></label>
								<p><textarea class="president_message form-control-lg" name="president_message"><?php echo $president_details['message'];?></textarea></p>
							</div>
						</div>

						<hr><br>

							<h4><b><i class="fas fa-edit"></i> PSP OVERALL CHAIRPERSON</b></h4><br>
							<div class="row">
								<div class="col-4">		
									<div class="form-group" style="border: solid grey;">
									<div class="card" style="background-color:#edd8c1;">
									    <br>
									    <label for="exampleInputFile" style="margin-left: 20px;"><h4><b>Update Display Picture</b></h4></label>
									    <input type="file" name="chair_dp" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" style="margin-left: 20px;">
									</div>
										<div class="card-body">
											<label><b>Fullname:</b></label>
											<p><input type="text" value="<?php echo $chair_details['fullname'];?>" name="chair_fullname" class="form-control"></p>
											<label><b>Position:</b></label>
											<p><input type="text" value="<?php echo $chair_details['position'];?>" name="chair_position" class="form-control"></p>
										</div>
									</div>
								</div> 
								<div class="col-8" style="border: solid grey;">
									<br>
									<label><b>Title:</b></label>
									<p><input type="text" name="chair_title" value="<?php echo $chair_details['title'];?>" class="form-control"></p>
									<label><b>Subtitle:</b></label>
									<p><input type="text" name="chair_subtitle" value="<?php echo $chair_details['subtitle'];?>" class="form-control"></p>
									<label><b>Message:</b></label>
									<p><textarea class="chair_message form-control" name="chair_message"><?php echo $chair_details['message'];?></textarea></p>
								</div>
							</div>
							<div class="row d-flex justify-content-end pt-3">
								<button type="submit" name="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>&nbsp
								<a href="welcome-message-admin.php" class="btn btn-secondary"><i class="fa fa-times"></i> Cancel</a>								
							</div>
					</div>
					<br>
				</form>
			</div>
		</div>
	</div>
					<div class="p-5">
						<?php include('./components-admin/footer-admin.php') ?>
					</div>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./js/jquery.richtext.min.js"></script>
    <script type="text/javascript">
    	$(function(){
    		$('.president_message').richText();
    		$('.chair_message').richText();

    	})
    </script>

  </body>
</html>
