<?php
include('./dbcon.php');

$mysqli = new mysqli($servername, $username, $password, $dbname);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

$query = "SELECT * FROM anatomic";
$result = $mysqli->query($query)

?>

<?php include('./components/header.php') ?>
	<div class="jumbotron mb-0 mt-3" id="bg1" style="padding-top: 220px;">
		<div class="row">
			<div class="col-lg-3 col-md-3 d-none d-sm-block pt-3">					
				<?php include('./components/sidebar.php') ?>	
			</div>

			<!-- ************//CONTENT//************ -->
			<div class="col-lg-8 col-md-8 col-sm-12" id="anatomic-content">
				<div class="content jumbotron mt-3 pt-4 pb-5 rounded-0" style="width: 1240px;">
				<img src="img/anatomic.jpg" id="banner" style="border-style: solid; border-color: grey;">
					<h4 class="title mt-3"><b>ANATOMIC PATHOLOGY SPEAKERS</b></h4>
					<hr style="width: 100%; opacity: 0.2;">
					<br>
					<div class="container">
						<?php 
						    while ($row = $result->fetch_assoc()) {								        
						 ?>		
							<div class="row mb-5">
								<div class="col-5">	
									<div class="card">
										<img class="card-img-top" src="<?php echo $row['file_path_dp'];?>" alt="Card image" style="width:100%">
										<div class="card-body">
											<h4 class="card-title" style="font-weight: bold;"><?php echo $row['fullname'] ?></h4>
											<h4 class="card-title"><?php echo $row['position'] ?></h4>
										</div>
									</div>
								</div> 

								<div class="col-7">
									<h3 style="font-weight: bold;"><?php echo $row['title'] ?></h3><br>
									<h5><?php echo $row['subtitle'] ?></h5><br>
									<?php echo $row['message'] ?>
								</div>
							</div> 
					        <?php
					   		}
					   	?>
					</div>		
						<br><hr><br>
				</div>
			</div>
		</div>
	</div>
	<div>
		<?php include('./components/footer.php') ?>
	</div>