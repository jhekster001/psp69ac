<?php
		
   	include('session.php');
	include('./dbcon.php');

	if(isset($_POST['submit'])){
		$about_title = addslashes($_POST['title']);
		$about_subtitle = addslashes($_POST['subtitle']);
		$about_message = addslashes($_POST['message']);	

		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}
		 //Upload
		$path = 'uploads/about-convention/';
		if(!file_exists($path)){
			mkdir('uploads/about-convention/', 0777, true);
		}
		$about_ext = pathinfo($_FILES["about_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["about_dp"]["name"] != ''){
			$about_target_file = $path . 'about-the-convention'.'.'.$about_ext;
			move_uploaded_file($_FILES["about_dp"]["tmp_name"], $about_target_file);		
		}else{
			$about_select_sql = 'SELECT * from about_the_convention';
			$result_about = $conn->query($about_select_sql);
			$about_details = $result_about->fetch_assoc();
			$about_target_file = $about_details['file_path_dp'];
		}

		$sql = "UPDATE about_the_convention SET 
				title='".$about_title."', 
				subtitle='".$about_subtitle."', 
				message='".$about_message."', 
				file_path_dp='".$about_target_file."'";

		if ($conn->query($sql) === TRUE) {
		    header("Location: about-the-convention-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 