<?php

   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$research_fullname = addslashes($_POST['fullname']);
		$research_position = addslashes($_POST['position']);
		$research_title = addslashes($_POST['title']);	

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}


		$sql = "INSERT INTO research SET 
				fullname='".$research_fullname."', 
				position='".$research_position."',
				title='".$research_title."', 
				file_path_dp=''";

		if ($conn->query($sql) === TRUE) {
		    $last_id = $conn->insert_id;
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		//Upload Manuscript
		$mpath = 'uploads/manuscripts/';
		if(!file_exists($mpath)){
			mkdir('uploads/manuscripts/', 0777, true);
		}
		$manuscript_ext = pathinfo($_FILES["pdffile"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["pdffile"]["name"] != ''){
			$manuscript_target_file = $mpath . 'manuscript_'.$last_id.'.'.$manuscript_ext;
			move_uploaded_file($_FILES["pdffile"]["tmp_name"], $manuscript_target_file);		
		}else{
			$manuscript_target_file = '';
		}

		//Upload
		$path = 'uploads/research/';
		if(!file_exists($path)){
			mkdir('uploads/research/', 0777, true);
		}
		$research_ext = pathinfo($_FILES["research_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["research_dp"]["name"] != ''){
			$research_target_file = $path . 'research-dp_'.$last_id.'.'.$research_ext;
			move_uploaded_file($_FILES["research_dp"]["tmp_name"], $research_target_file);		
		}else{
			$research_target_file = '';
		}		 

		$update_sql = "UPDATE research SET 
						file_path_dp='".$research_target_file."',
						manuscript_file_path='".$manuscript_target_file."'
						WHERE id='".$last_id."'";

		if ($conn->query($update_sql) === TRUE) {
			header("Location: research-presentations-admin.php");
		} else {
		    echo "Error: " . $update_sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 
