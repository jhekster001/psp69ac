<?php
   include('session.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/psp.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <title>PSP Admin</title>
  </head>
<body>

<?php
include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM election";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
		$elec_details = $result->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();
?>

	<?php include('./components-admin/header-admin.php') ?>
		<div class="jumbotron bg-light mb-0 pb-0 mt-3">
			<div class="row">
				<div class="col-lg-2 d-none d-sm-block">					
					<?php include('./components-admin/sidebar-admin.php') ?>	
				</div>
					<!-- ************//PAGE//************ -->
					<div class="content col-lg-8 col-sm-12 mt-3 pt-3" id="home-content">
						<img src="img/election.jpg" id="banner" style="border-style: solid; border-color: grey;">
						<h4 class="title mt-3"><b>ELECTION FOR BOARD OF GOVERNORS</b></h4>
						<hr style="width: 100%; opacity: 0.2;">
						<br>
						<div class="container">
						<h5 style="color: green">Note: Updating image may have delay. Please refresh the page <kbd>(ctrl+F5)</kbd> to see results.</h5><br>
						<div class="row">
							<div class="col-5">		
								<div class="card">
										<img class="card-img-top" src="<?php echo $elec_details['file_path_dp'];?>" alt="Card image" style="width:100%">
									<div class="card-body">
										<h4 class="card-title"><?php echo $elec_details['fullname'] ?></h4>
										<p class="card-text"><?php echo $elec_details['position'] ?></p>
									</div>
								</div>
							</div> 
							<div class="col-7">
								<a href="election-for-board-of-governors-edit.php" class="btn btn-success"><i class="fa fa-edit "></i> Edit</a>
								<h3><?php echo $elec_details['title'] ?></h3>
								<br>
								<h5><?php echo $elec_details['subtitle'] ?></h5>
								<br>
								<?php echo $elec_details['message'] ?>
							</div>
						</div>		
					<br><hr><br>
					</div>
					<div class="container">
						<h4><b>ANNOUNCEMENT FROM THE COMELEC</b></h4>
						<img class="howto" src="<?php echo $elec_details['file_path_img'];?>">
						<br><hr>
						<h3><?php echo $elec_details['title_2'] ?></h3>
						<h4><?php echo $elec_details['subtitle_2'] ?></h4>
						<?php echo $elec_details['message_2'] ?>
						</div>
						<br>
					</div>
			</div>

		</div>
		<div class="p-5">
			<?php include('./components-admin/footer-admin.php') ?>
		</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>