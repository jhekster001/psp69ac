<footer>
	<img class="imgfooter" src="img/footer_02.jpg">
	<div>
		<h6 class="bottomtext text-center" style="text-shadow: 2px 2px black;">			
			<a class="lbottom" href="index.php">Home </a> |
			<a class="lbottom" href="registration.php">Registration </a> |
			<a class="lbottom" href="contact-the-psp-secretariat.php">Contact the PSP Secretariat </a> |
			<a class="lbottom" href="javascript:void(0)" data-toggle="modal" data-target="#login">Sign In</a>
			<br>
			<br >Philippine Society of Pathologists, Inc. 
			<br>All Rights Reserved 2020
		</h6>
	</div>
</footer>
    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->

    <!-- CDNs -->
	<!-- <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script> -->
    <script type="text/javascript" src="js/jquery.min.js"></script>
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="./js/script.js"></script>
<!--     <script type="text/javascript">
    	// window.onload = function(){
    	// 	$('#navbtn button').click(function(){
    	// 		$('.content').hide();
    	// 		console.log($(this).attr('id'))
    	// 		$('.content#'+$(this).attr('id')).show()
    	// 	})
    	// }

		$(document).ready(function() {

			$('#btnlogin').click(function(event) {
		        event.preventDefault();
		       	var ValidUsername = $('#username').val() === 'admin';
				var ValidPassword = $('#password').val() === 'admin';

		        if (ValidUsername === true && ValidPassword === true) {
		            window.location = "psp69admin.html";
		        }
		        else {
		            $("#invalid").show();
		        }
		    });

		    $('#btnclose').click(function() {
		    		$("#invalid").hide();
		    		$("#username").val("");
		    		$("#password").val("");
		    });
		});

		// $(function(){
		// 	$("#seemore").click(function(e){
		// 		e.preventDefault();
		// 		$(".content").hide();
		// 		$(".content#psp-about").show();
		// 	})
		// })
    </script> -->
	<script type="text/javascript">
		$(function(){
			$('#myVideo').attr('src', '')
			$('#modalVideoBtn').click(function(){
				$('#myVideo').attr('src', 'img/intro.mp4')
			});

			$('#video').on('hide.bs.modal', function(){
				$('#myVideo').attr('src', '')
			});

			// $('#closeModal').click(function(){
			// 	$('#myVideo').attr('src', '')
			// });


		})
	</script>    
  </body>
</html>