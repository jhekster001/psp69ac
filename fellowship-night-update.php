<?php
   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$fellowship_title = addslashes($_POST['title']);
		$fellowship_subtitle = addslashes($_POST['subtitle']);
		$fellowship_message = addslashes($_POST['message']);	

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		//Upload
		$path = 'uploads/fellowship/';
		if(!file_exists($path)){
			mkdir('uploads/fellowship/', 0777, true);
		}
		$fellowship_ext = pathinfo($_FILES["fellowship_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["fellowship_dp"]["name"] != ''){
			$fellowship_target_file = $path . 'fellowship'.'.'.$fellowship_ext;
			move_uploaded_file($_FILES["fellowship_dp"]["tmp_name"], $fellowship_target_file);		
		}else{
			$fellowship_select_sql = 'SELECT * from fellowship';
			$result_fellowship = $conn->query($fellowship_select_sql);
			$fellowship_details = $result_fellowship->fetch_assoc();
			$fellowship_target_file = $fellowship_details['file_path_dp'];
		}

		$sql = "UPDATE fellowship SET 
				title='".$fellowship_title."', 
				subtitle='".$fellowship_subtitle."', 
				message='".$fellowship_message."',
				file_path_dp='".$fellowship_target_file."'";

		if ($conn->query($sql) === TRUE) {
			header("Location: fellowship-night-admin.php");
		} else {
		    echo "Errors: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

 ?>