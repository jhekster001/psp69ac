<?php

   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$clinical_id = addslashes($_POST['clinical_id']);
		$clinical_fullname = addslashes($_POST['fullname']);
		$clinical_position = addslashes($_POST['position']);
		$clinical_title = addslashes($_POST['title']);
		$clinical_subtitle = addslashes($_POST['subtitle']);
		$clinical_message = addslashes($_POST['message']);		

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		//Upload
		$path = 'uploads/clinical/';
		if(!file_exists($path)){
			mkdir('uploads/clinical/', 0777, true);
		}
		$clinical_ext = pathinfo($_FILES["clinical_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["clinical_dp"]["name"] != ''){
			$clinical_target_file = $path . 'clinical-dp_'.$clinical_id.'.'.$clinical_ext;
			move_uploaded_file($_FILES["clinical_dp"]["tmp_name"], $clinical_target_file);		
		}else{
			$clinical_select_sql = 'SELECT * from clinical WHERE id="'.$clinical_id.'" ';
			$result_clinical = $conn->query($clinical_select_sql);
			$clinical_details = $result_clinical->fetch_assoc();
			$clinical_target_file = $clinical_details['file_path_dp'];
		}

		$sql = "UPDATE clinical SET 
				fullname='".$clinical_fullname."', 
				position='".$clinical_position."',
				title='".$clinical_title."', 
				subtitle='".$clinical_subtitle."', 
				message='".$clinical_message."', 
				file_path_dp='".$clinical_target_file."'
				WHERE id='".$clinical_id."'";

		if ($conn->query($sql) === TRUE) {
			header("Location: clinical-pathology-speakers-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 
