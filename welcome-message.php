<?php
include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$pres_sql = "SELECT * FROM welcome_message WHERE type='president'";
$chair_sql = "SELECT * FROM welcome_message WHERE type='chair'";
$result_pres = $conn->query($pres_sql);
$result_chair = $conn->query($chair_sql);

if ($result_pres->num_rows > 0) {
		$president_details = $result_pres->fetch_assoc();
} else {
    echo "0 results";
}
if ($result_chair->num_rows > 0) {
		$chair_details = $result_chair->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();
?>	

<?php include('./components/header.php') ?>
	<div class="jumbotron mb-0 mt-3" id="bg1" style="padding-top: 220px;">
		<div class="row">
			<div class="col-lg-3 col-md-3 d-none d-sm-block pt-3">					
				<?php include('./components/sidebar.php') ?>	
			</div>

			<!-- ************//CONTENT//************ -->
			<div class="col-lg-8 col-md-8 col-sm-12" id="welcome-content">
				<div class="content jumbotron mt-3 pt-4 pb-5 rounded-0" style="width: 1240px;">
					<img src="img/welcome.jpg" id="banner" style="border-style: solid; border-color: grey;">
					<h4 class="title mt-3"><b>WELCOME MESSAGE</b></h4>
					<hr style="width: 100%; opacity: 0.2;">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-5">		
								<div class="card">
										<img class="card-img-top" src="<?php echo $president_details['file_path_dp'];?>" alt="Card image" style="width:100%">
									<div class="card-body">
										<h4 class="card-title" style="font-weight: bold;"><?php echo $president_details['fullname'] ?></h4>
										<h4 class="card-text"><?php echo $president_details['position'] ?></h4>
									</div>
								</div>
							</div> 
							<div class="col-7">
								<h3 style="font-weight: bold;"><?php echo $president_details['title'] ?></h3>
								<br>
								<h5><?php echo $president_details['subtitle'] ?></h5>
								<br>
								<?php echo $president_details['message'] ?>
							</div>
						</div>		
						<br><hr><br>
					</div>
					<div class="container">
						<div class="row">
							<div class="col-5">		
								<div class="card">
										<img class="card-img-top" src="<?php echo $chair_details['file_path_dp'];?>" alt="Card image" style="width:100%">
									<div class="card-body">
										<h4 class="card-title" style="font-weight: bold;"><?php echo $chair_details['fullname'] ?></h4>
										<h4 class="card-text"><?php echo $chair_details['position'] ?></h4>
									</div>
								</div>
							</div> 
							<div class="col-7">
								<h3 style="font-weight: bold;"><?php echo $chair_details['title'] ?></h3>
								<br>
								<h5><?php echo $chair_details['subtitle'] ?></h5>
								<br>
								<?php echo $chair_details['message'] ?>
							</div>
						</div>		
						<br><hr><br>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
	<div>
		<?php include('./components/footer.php') ?>
	</div>