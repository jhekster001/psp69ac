<?php
include('session.php');
include('./dbcon.php');

	if(isset($_POST['submit'])){
		$announcements_id = addslashes($_POST['announcements_id']);
		$announcements_title = addslashes($_POST['title']);
		$announcements_subtitle = addslashes($_POST['subtitle']);
		$announcements_message = addslashes($_POST['message']);		

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		//Upload
		$path = 'uploads/announcements/';
		if(!file_exists($path)){
			mkdir('uploads/announcements/', 0777, true);
		}
		$announcements_ext = pathinfo($_FILES["announcements_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["announcements_dp"]["name"] != ''){
			$announcements_target_file = $path . 'announcements-dp_'.$announcements_id.'.'.$announcements_ext;
			move_uploaded_file($_FILES["announcements_dp"]["tmp_name"], $announcements_target_file);		
		}else{
			$announcements_select_sql = 'SELECT * from announcements WHERE id="'.$announcements_id.'" ';
			$result_announcements = $conn->query($announcements_select_sql);
			$announcements_details = $result_announcements->fetch_assoc();
			$announcements_target_file = $announcements_details['file_path_dp'];
		}

		$sql = "UPDATE announcements SET 
				title='".$announcements_title."', 
				subtitle='".$announcements_subtitle."', 
				message='".$announcements_message."', 
				file_path_dp='".$announcements_target_file."'
				WHERE id='".$announcements_id."'";

		if ($conn->query($sql) === TRUE) {
			header("Location: announcements-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}
?>