<?php
include('./dbcon.php');

$mysqli = new mysqli($servername, $username, $password, $dbname);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}

$query = "SELECT * FROM announcements";
$result = $mysqli->query($query)

?>

<?php include('./components/header.php') ?>
	<div class="jumbotron mb-0 mt-3" id="bg1" style="padding-top: 220px;">
		<div class="row">
			<div class="col-lg-3 col-md-3 d-none d-sm-block pt-3">					
				<?php include('./components/sidebar.php') ?>	
			</div>

			<!-- ************//CONTENT//************ -->
			<div class="col-lg-8 col-md-8 col-sm-12" id="announcements-content">
				<div class="content jumbotron mt-3 pt-4 pb-5 rounded-0" style="width: 1240px;">
				<img src="img/announcement.jpg" id="banner" style="border-style: solid; border-color: grey;">
					<h4 class="title mt-3"><b>ANNOUNCEMENTS</b></h4>
					<hr style="width: 100%; opacity: 0.2;">
					
					<div class="container">
						<?php 
						    while ($row = $result->fetch_assoc()) {								        
						 ?>		
							<div class="row mb-2">
								<div class="col-12">
									<h3 style="font-weight: bold"><?php echo $row['title'] ?></h3><br>
									<h5><?php echo $row['subtitle'] ?></h5><br>
									<?php echo $row['message'] ?>
									<hr>
								</div>								
									<div class="mt-3" >
									<img class="howto" src="<?php echo $row['file_path_dp'];?>">									
									</div>									
								<hr style="width: 100%;">
							</div> 
					        <?php
					   		}
					   	?>
					</div>		
				</div>
			</div>
		</div>
	</div>
	<div>
		<?php include('./components/footer.php') ?>
	</div>