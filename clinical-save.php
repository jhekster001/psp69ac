<?php

   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$clinical_fullname = addslashes($_POST['fullname']);
		$clinical_position = addslashes($_POST['position']);
		$clinical_title = addslashes($_POST['title']);
		$clinical_subtitle = addslashes($_POST['subtitle']);
		$clinical_message = addslashes($_POST['message']);		

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		$sql = "INSERT INTO clinical SET 
				fullname='".$clinical_fullname."', 
				position='".$clinical_position."',
				title='".$clinical_title."', 
				subtitle='".$clinical_subtitle."', 
				message='".$clinical_message."', 
				file_path_dp=''";

		if ($conn->query($sql) === TRUE) {
		    $last_id = $conn->insert_id;
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		//Upload
		$path = 'uploads/clinical/';
		if(!file_exists($path)){
			mkdir('uploads/clinical/', 0777, true);
		}
		$clinical_ext = pathinfo($_FILES["clinical_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["clinical_dp"]["name"] != ''){
			$clinical_target_file = $path . 'clinical-dp_'.$last_id.'.'.$clinical_ext;
			move_uploaded_file($_FILES["clinical_dp"]["tmp_name"], $clinical_target_file);		
		}else{
			$clinical_target_file = '';
		}		

		$update_sql = "UPDATE clinical SET 
						file_path_dp='".$clinical_target_file."' 
						WHERE id='".$last_id."'";

		if ($conn->query($update_sql) === TRUE) {
			header("Location: clinical-pathology-speakers-admin.php");
		} else {
		    echo "Error: " . $update_sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 
