<?php
   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$committee_fullname = addslashes($_POST['fullname']);
		$committee_position = addslashes($_POST['position']);

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		$sql = "INSERT INTO committee SET 
				fullname='".$committee_fullname."', 
				position='".$committee_position."',
				file_path_dp=''";


		if ($conn->query($sql) === TRUE) {
			$last_id = $conn->insert_id; 
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		//Upload
		$path = 'uploads/committee/';
		if(!file_exists($path)){
			mkdir('uploads/committee/', 0777, true);
		}
		$committee_ext = pathinfo($_FILES["committee_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["committee_dp"]["name"] != ''){
			$committee_target_file = $path . 'committee-dp_'.$last_id.'.'.$committee_ext;
			move_uploaded_file($_FILES["committee_dp"]["tmp_name"], $committee_target_file);		
		}else{
			$committee_target_file = '';
		}

		$update_sql = "UPDATE committee SET 
						file_path_dp='".$committee_target_file."' 
						WHERE id='".$last_id."'";

		if ($conn->query($update_sql) === TRUE) {
			header("Location: organizing-committee-admin.php");
		} else {
		    echo "Error: " . $update_sql . "<br>" . $conn->error;
		}				


		$conn->close();	
	}

	
 ?>

 