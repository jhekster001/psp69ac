<?php

include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM about_the_convention";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
		$about_details = $result->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();
?>

<?php include('./components/header.php') ?>
	<div class="jumbotron mb-0 mt-3" id="bg1" style="padding-top: 220px;">
		<div class="row">
			<div class="col-lg-3 col-md-3 d-none d-sm-block pt-3">					
				<?php include('./components/sidebar.php') ?>	
			</div>

			<!-- ************//CONTENT//************ -->
			<div class="col-lg-8 col-md-8 col-sm-12 " id="about-content">
				<div class="content jumbotron mt-3 pt-4 pb-5 rounded-0" style="width: 1240px;">	
				<img src="img/convention.jpg" id="banner" style="border-style: solid; border-color: grey;">
				<h4 class="title mt-3"><b>ABOUT THE CONVENTION</b></h4>
				<hr style="width: 100%; opacity: 0.2;">
				<br>
				<div class="container">
					<div class="row">
						<div class="col-6">		
							<div class="card">
								<img class="card-img-top" src="<?php echo $about_details['file_path_dp'];?>" alt="Card image" style="width:100%; border: solid #c0a383;">
							</div>
						</div>
						<div class="col-6">
							<h3 style="font-weight: bold;"><?php echo $about_details['title']; ?></h3>
							<h4><?php echo $about_details['subtitle']; ?></h4>
							<br>
							<?php echo $about_details['message']; ?>
						</div>
					</div>		
				<br>
				</div>
			</div>
		</div>
	</div>
</div>

	<div>
		<?php include('./components/footer.php') ?>
	</div>