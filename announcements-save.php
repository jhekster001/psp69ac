<?php
include('session.php');
include('./dbcon.php');

	if(isset($_POST['submit'])){
		$announcements_title = addslashes($_POST['title']);
		$announcements_subtitle = addslashes($_POST['subtitle']);
		$announcements_message = addslashes($_POST['message']);		

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		$sql = "INSERT INTO announcements SET 
				title='".$announcements_title."', 
				subtitle='".$announcements_subtitle."', 
				message='".$announcements_message."', 
				file_path_dp=''";

		if ($conn->query($sql) === TRUE) {
		    $last_id = $conn->insert_id;
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		//Upload
		$path = 'uploads/announcements/';
		if(!file_exists($path)){
			mkdir('uploads/announcements/', 0777, true);
		}
		$announcements_ext = pathinfo($_FILES["announcements_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["announcements_dp"]["name"] != ''){
			$announcements_target_file = $path . 'announcements-dp_'.$last_id.'.'.$announcements_ext;
			move_uploaded_file($_FILES["announcements_dp"]["tmp_name"], $announcements_target_file);		
		}else{
			$announcements_target_file = '';
		}		

		$update_sql = "UPDATE announcements SET 
						file_path_dp='".$announcements_target_file."' 
						WHERE id='".$last_id."'";

		if ($conn->query($update_sql) === TRUE) {
			header("Location: announcements-admin.php");
		} else {
		    echo "Error: " . $update_sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}
?>