<?php

   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$research_id = addslashes($_POST['research_id']);
		$research_fullname = addslashes($_POST['fullname']);
		$research_position = addslashes($_POST['position']);
		$research_title = addslashes($_POST['title']);
		

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		//Update Manuscript
		$mpath = 'uploads/research/';
		if(!file_exists($mpath)){
			mkdir('uploads/research/', 0777, true);
		}
		$manuscript_ext = pathinfo($_FILES["pdffile"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["pdffile"]["name"] != ''){
			$manuscript_target_file = $mpath . 'manuscript_'.$research_id.'.'.$manuscript_ext;
			move_uploaded_file($_FILES["pdffile"]["tmp_name"], $manuscript_target_file);		
		}else{
			$research_select_sql = 'SELECT * from research WHERE id="'.$research_id.'" ';
			$result_research = $conn->query($research_select_sql);
			$research_details = $result_research->fetch_assoc();
			$manuscript_target_file = $research_details['manuscript_file_path'];
		}

		//Update Photo
		$path = 'uploads/research/';
		if(!file_exists($path)){
			mkdir('uploads/research/', 0777, true);
		}
		$research_ext = pathinfo($_FILES["research_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["research_dp"]["name"] != ''){
			$research_target_file = $path . 'research-dp_'.$research_id.'.'.$research_ext;
			move_uploaded_file($_FILES["research_dp"]["tmp_name"], $research_target_file);		
		}else{
			$research_select_sql = 'SELECT * from research WHERE id="'.$research_id.'" ';
			$result_research = $conn->query($research_select_sql);
			$research_details = $result_research->fetch_assoc();
			$research_target_file = $research_details['file_path_dp'];
		}


		$sql = "UPDATE research SET 
				fullname='".$research_fullname."', 
				position='".$research_position."',
				title='".$research_title."', 
				manuscript_file_path='".$manuscript_target_file."',
				file_path_dp='".$research_target_file."'
				WHERE id='".$research_id."'";

		if ($conn->query($sql) === TRUE) {
			header("Location: research-presentations-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 
