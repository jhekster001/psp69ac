<?php
include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM convention_venue";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
		$venue_details = $result->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();
?>

<?php include('./components/header.php') ?>
	<div class="jumbotron mb-0 mt-3" id="bg1" style="padding-top: 220px;">
		<div class="row">
			<div class="col-lg-3 col-md-3 d-none d-sm-block pt-3">					
				<?php include('./components/sidebar.php') ?>	
			</div>

			<!-- ************//CONTENT//************ -->
			<div class="col-lg-8 col-md-8 col-sm-12" id="venue-content">
				<div class="content jumbotron mt-3 pt-4 pb-5 rounded-0" style="width: 1240px;">
				<img src="img/conrad.jpg" id="banner" style="border-style: solid; border-color: grey;">
				<h4 class="title mt-3"><b>CONVENTION VENUE</b></h4>
				<hr style="width: 100%; opacity: 0.2;">

				<h3 style="font-weight: bold;"><?php echo $venue_details['title']; ?></h3>
				<h4><?php echo $venue_details['subtitle']; ?></h4>
				<?php echo $venue_details['message']; ?>
			</div>
		</div>
	</div>
</div>
	<div>
		<?php include('./components/footer.php') ?>
	</div>