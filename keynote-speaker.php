<?php
include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$opening_sql = "SELECT * FROM keynote_speaker WHERE type='opening'";
$closing_sql = "SELECT * FROM keynote_speaker WHERE type='closing'";
$result_opening = $conn->query($opening_sql);
$result_closing = $conn->query($closing_sql);

if ($result_opening->num_rows > 0) {
		$opening_details = $result_opening->fetch_assoc();
} else {
    echo "0 results";
}
if ($result_closing->num_rows > 0) {
		$closing_details = $result_closing->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();
?>	

<?php include('./components/header.php') ?>
	<div class="jumbotron mb-0 mt-3" id="bg1" style="padding-top: 220px;">
		<div class="row">
			<div class="col-lg-3 col-md-3 d-none d-sm-block pt-3">					
				<?php include('./components/sidebar.php') ?>	
			</div>

			<!-- ************//CONTENT//************ -->
			<div class="col-lg-8 col-md-8 col-sm-12" id="keynote-content">
				<div class="content jumbotron mt-3 pt-4 pb-5 rounded-0" style="width: 1240px;">
				<img src="img/keynote.jpg" id="banner" style="border-style: solid; border-color: grey;">
					<h4 class="title mt-3"><b>KEYNOTE SPEAKER</b></h4>
					<hr style="width: 100%; opacity: 0.2;">
					<br>
					<div class="container">
						<div class="row">
							<div class="col-5">		
								<div class="card">
									<img class="card-img-top" src="<?php echo $opening_details['file_path_dp'];?>" alt="Card image" style="width:100%">
									<div class="card-body">
										<h4 class="card-title" style="font-weight: bold;"><?php echo $opening_details['fullname'] ?></h4>
										<h4 class="card-text"><?php echo $opening_details['position'] ?></h4>
									</div>
								</div>
							</div> 
							<div class="col-7">
								<h3 style="font-weight: bold;"><?php echo $opening_details['title'] ?></h3>
								<br>
								<h5><?php echo $opening_details['subtitle'] ?></h5>
								<br>
								<?php echo $opening_details['message'] ?>
							</div>
						</div>		
							<br><hr><br>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-5">		
									<div class="card">
										<img class="card-img-top" src="<?php echo $closing_details['file_path_dp'];?>" alt="Card image" style="width:100%">
										<div class="card-body">
											<h4 class="card-title" style="font-weight: bold;"><?php echo $closing_details['fullname'] ?></h4>
											<h4 class="card-text"><?php echo $closing_details['position'] ?></h4>
										</div>
									</div>
								</div> 
								<div class="col-7">
									<h3 style="font-weight: bold;"><?php echo $closing_details['title'] ?></h3>
									<br>
									<h5><?php echo $closing_details['subtitle'] ?></h5>
									<br>
									<?php echo $closing_details['message'] ?>
								</div>
							</div>		
							
						</div>		
					<br><hr><br>
					</div>
				</div>

		</div>
	</div>
</div>
	<div>
		<?php include('./components/footer.php') ?>
	</div>