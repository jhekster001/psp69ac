<?php
   include('session.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/psp.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <!-- RTE -->
    <link rel="stylesheet" type="text/css" href="./css/richtext.min.css">

    <title>PSP Admin</title>
  </head>
<body>

<?php
include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$opening_sql = "SELECT * FROM keynote_speaker WHERE type='opening'";
$closing_sql = "SELECT * FROM keynote_speaker WHERE type='closing'";
$result_opening = $conn->query($opening_sql);
$result_closing = $conn->query($closing_sql);

if ($result_opening->num_rows > 0) {
		$opening_details = $result_opening->fetch_assoc();
} else {
    echo "0 results";
}
if ($result_closing->num_rows > 0) {
		$closing_details = $result_closing->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();
?>

	<?php include('./components-admin/header-admin.php') ?>
		<div class="jumbotron bg-light mb-0 pb-0 mt-3">
			<div class="row">
				<div class="col-lg-2 d-none d-sm-block">					
					<?php include('./components-admin/sidebar-admin.php') ?>	
				</div>
					<!-- ************//HOME PAGE//************ -->
					<div class="content col-lg-8 col-sm-12 mt-3 pt-3" id="home-content">
						<img src="img/keynote.jpg" id="banner" style="border-style: solid; border-color: grey;">
						<h4 class="title mt-3"><b><i class="fas fa-edit"></i> EDIT KEYNOTE SPEAKER</b></h4>
						<hr style="width: 100%; opacity: 0.2;">
						<br>
					<form method="post" enctype="multipart/form-data" action="keynote_speaker_update.php">
							<div class="container">
						<h4 class="title mt-3"><b><i class="fas fa-edit"></i> EDIT OPENING SPEAKER DETAILS</b></h4><br>
						<div class="row">
							<div class="col-4">		
								<div class="form-group" style="border: solid grey;">
								<div class="card" style="background-color:#edd8c1;">
								    <br>
								    <label for="exampleInputFile" style="margin-left: 20px;"><h4><b>Update Display Picture</b></h4>
									<p style="color: green;"><b>Note: Photo must be in "3 x 4" inches size (recommended)</b></p>
								    </label>
								    <input type="file" name="opening_dp" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" style="margin-left: 20px;">
								</div>
									<div class="card-body">
										<label><b>Fullname:</b></label>
										<p><input type="text" value="<?php echo $opening_details['fullname'];?>" name="opening_fullname" class="form-control"></p>
										<label><b>Position:</b></label>
										<p><input type="text" value="<?php echo $opening_details['position'];?>" name="opening_position" class="form-control"></p>
									</div>
								</div>
							</div> 
							<div class="col-8" style="border: solid grey;"><br>							
								<label><b>Title:</b></label>
								<p><input type="text" value="<?php echo $opening_details['title'];?>" name="opening_title" class="form-control"></p>
								<label><b>Subtitle:</b></label>
								<p><input type="text" value="<?php echo $opening_details['subtitle'];?>" name="opening_subtitle" class="form-control"></p>
								<label><b>Message:</b></label>
								<p><textarea class="opening_message form-control-lg" name="opening_message"><?php echo $opening_details['message'];?></textarea></p>
							</div>
						</div>

						<hr><br>

							<h4><b><i class="fas fa-edit"></i> EDIT CLOSING SPEAKER DETAILS</b></h4><br>
							<div class="row">
								<div class="col-4">		
									<div class="form-group" style="border: solid grey;">
									<div class="card" style="background-color:#edd8c1;">
									    <br>
									    <label for="exampleInputFile" style="margin-left: 20px;"><h4><b>Update Display Picture</b></h4></label>
									    <input type="file" name="closing_dp" class="form-control-file" id="exampleInputFile" aria-describedby="fileHelp" style="margin-left: 20px;">
									</div>
										<div class="card-body">
											<label><b>Fullname:</b></label>
											<p><input type="text" value="<?php echo $closing_details['fullname'];?>" name="closing_fullname" class="form-control"></p>
											<label><b>Position:</b></label>
											<p><input type="text" value="<?php echo $closing_details['position'];?>" name="closing_position" class="form-control"></p>
										</div>
									</div>
								</div> 
								<div class="col-8" style="border: solid grey;">
									<br>
									<label><b>Title:</b></label>
									<p><input type="text" name="closing_title" value="<?php echo $closing_details['title'];?>" class="form-control"></p>
									<label><b>Subtitle:</b></label>
									<p><input type="text" name="closing_subtitle" value="<?php echo $closing_details['subtitle'];?>" class="form-control"></p>
									<label><b>Message:</b></label>
									<p><textarea class="closing_message form-control" name="closing_message"><?php echo $closing_details['message'];?></textarea></p>
								</div>
							</div>
							<div class="row d-flex justify-content-end pt-3">
								<button type="submit" name="submit" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>&nbsp
								<a href="keynote-speaker-admin.php" class="btn btn-secondary"><i class="fa fa-times"></i> Cancel</a>								
							</div>
					</div>
							<br>
						</form>
			</div>
		</div>
		<div class="p-5">
			<?php include('./components-admin/footer-admin.php') ?>
		</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
    <script type="text/javascript" src="./js/jquery.richtext.min.js"></script>
    <script type="text/javascript">
    	$(function(){
    		$('.opening_message').richText();
    		$('.closing_message').richText();

    	})
    </script>

  </body>
</html>