<?php
   include('session.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/psp.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <title>PSP Admin</title>
  </head>
<body>
	
<?php
include('./dbcon.php');

$mysqli = new mysqli($servername, $username, $password, $dbname);

/* check connection */
if ($mysqli->connect_errno) {
    printf("Connect failed: %s\n", $mysqli->connect_error);
    exit();
}
$sql = "SELECT * FROM research";
$result = $mysqli->query($sql)

?>
	<?php include('./components-admin/header-admin.php') ?>
		<div class="jumbotron bg-light mb-0 pb-0 mt-3">
			<div class="row">
				<div class="col-lg-2 d-none d-sm-block">					
					<?php include('./components-admin/sidebar-admin.php') ?>	
				</div>
					<!-- ************// PAGE//************ -->
					<div class="content col-lg-8 col-sm-12 mt-3 pt-3" id="home-content">
						<img src="img/research.jpg" id="banner" style="border-style: solid; border-color: grey;">
						<h4 class="title mt-3"><b>RESEARCH PRESENTATIONS</b></h4>
						<hr style="width: 100%; opacity: 0.2;">
						<br>
						<div class="container">
							<a href="research-presentations-create.php" class="btn btn-primary mb-4"><b><i class="fas fa-plus"></i> Add Author</a>
							<h5 style="color: green">Note: Updating image may have delay. Please refresh the page <kbd>ctrl+F5</kbd> to see results.</h5><br>
							<?php 
							     while ($row = $result->fetch_assoc()) {	
							 ?>		
								<div class="row mb-5">
									<div class="col-5">	
										<div class="card">
											<img class="card-img-top" src="<?php echo $row['file_path_dp'];?>" alt="Card image" style="width:100%">
											<div class="card-body">
												<h4 class="card-title"><?php echo $row['fullname'] ?></h4>
												<p class="card-title"><?php echo $row['position'] ?></p>
											</div>
										</div>
									</div> 

									<div class="col-7" style="border: solid grey;">												
										<a href="research-presentations-edit.php?id=<?php echo $row['id'] ?> " class="btn btn-success mt-3"><i class="fa fa-edit "></i> Edit</a>&nbsp
										<a href="research-presentations-delete.php?id=<?php echo $row['id'] ?> " class="btn btn-danger mt-3"><i class="fa fa-edit "></i> Delete</a>

										<?php if($row['manuscript_file_path'] != ''){ ?>
											<div class="mt-3">
											  <iframe src="<?php echo $row['manuscript_file_path'] ?>" style="width:100%;height:500px;"></iframe>
											</div>								
										<?php
											}else{ 
										 ?>
										 	<p class="lead text-danger">
										 		No Manuscript uploaded
										 	</p>
										 <?php } ?>
									</div>
								</div> 
						        <?php
						   		}
						   	?>		
								<br><hr><br>
						</div>
					</div>
			</div>
		</div>
					<div class="p-5">
						<?php include('./components-admin/footer-admin.php') ?>
					</div>

    <!-- Optional JavaScript -->
    <!-- jQuery first, then Popper.js, then Bootstrap JS -->
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>