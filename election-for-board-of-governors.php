<?php
include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM election";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
		$elec_details = $result->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();
?>

<?php include('./components/header.php') ?>
	<div class="jumbotron mb-0 mt-3" id="bg1" style="padding-top: 220px;">
		<div class="row">
			<div class="col-lg-3 col-md-3 d-none d-sm-block pt-3">					
				<?php include('./components/sidebar.php') ?>	
			</div>

			<!-- ************//CONTENT//************ -->
			<div class="col-lg-8 col-md-8 col-sm-12" id="election-content">
				<div class="content jumbotron mt-3 pt-4 pb-5 rounded-0" style="width: 1240px;">
				<img src="img/election.jpg" id="banner" style="border-style: solid; border-color: grey;">
					<h4 class="title mt-3"><b>ELECTION FOR BOARD OF GOVERNORS</b></h4>
					<hr style="width: 100%; opacity: 0.2;">
					<br>
					<div class="container">
					<div class="row">
						<div class="col-5">		
							<div class="card">
									<img class="card-img-top" src="<?php echo $elec_details['file_path_dp'];?>" alt="Card image" style="width:100%">
								<div class="card-body">
									<h4 class="card-title" style="font-weight: bold;"><?php echo $elec_details['fullname'] ?></h4>
									<h4 class="card-text"><?php echo $elec_details['position'] ?></h4>
								</div>
							</div>
						</div> 
						<div class="col-7">
							<h3 style="font-weight: bold;"><?php echo $elec_details['title'] ?></h3>
							<br>
							<h5><?php echo $elec_details['subtitle'] ?></h5>
							<br>
							<?php echo $elec_details['message'] ?>
						</div>
					</div>		
				<br><hr><br>
				</div>
				<div class="container">
					<h3>Attachment here</h3>
					<img class="howto" src="<?php echo $elec_details['file_path_img'];?>">
					<br><hr>
					<h3 style="font-weight: bold;"><?php echo $elec_details['title_2'] ?></h3>
					<h4><?php echo $elec_details['subtitle_2'] ?></h4>
					<?php echo $elec_details['message_2'] ?>
					</div>
					<br>
				</div>
		</div>

	</div>
</div>
	<div>
		<?php include('./components/footer.php') ?>
	</div>