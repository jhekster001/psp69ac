<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="topnav" style="background-color: white !important;">
	<img class="imgheader" src="img/admin-header.jpg">
	<a class="navbar-brand" href="psp69ac.php"></a>
	<div class="container col-8">
		<button class="navbar-toggler bg-dark d-xl-none d-lg-none d-md-none" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation" style="outline: none">
		<span class="navbar-toggler-icon"></span>
		</button>
			<div class="collapse navbar-collapse" id="navbarColor02">
				<ul class="navbar-nav mr-auto d-xl-none d-lg-none d-md-none">

			    	<li class="nav-item active">
						<a class="nav-link" href="index-admin.php" style="color: grey;">Home</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="welcome-message-admin.php" style="color: grey;">Welcome Message</a>
					</li>
					<li class="nav-item">
						<a class="nav-link" href="about-the-convention-admin.php" style="color: grey;">About the Convention</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="convention-venue-admin.php" style="color: grey;">Convention Venue</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="announcements-admin.php" style="color: grey;">Announcements</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="convention-house-rules-admin.php" style="color: grey;">Convention House Rules</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="attendance-admin.php" style="color: grey;">Attendance</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="organizing-committee-admin.php" style="color: grey;">Organizing Committee</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="keynote-speaker-admin.php" style="color: grey;">Keynote Speaker</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="awards-admin.php" style="color: grey;">Awards</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="program-admin.php" style="color: grey;">Program</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="anatomic-pathology-speaker-admin.php" style="color: grey;">Anatomic Pathology Speakers</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="clinical-pathology-speakers-admin.php" style="color: grey;">Clinical Pathology Speakers</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="registration-admin.php" style="color: grey;">Registration</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="election-for-board-of-governors-admin.php" style="color: grey;">Election for Board of Govervors</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="congress-sponsors-admin.php" style="color: grey;">Congress Sponsors</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="the-psp-secretariat-admin.php" style="color: grey;">The PSP Secretariat</a>
					</li>
					<li class="nav-item">
					<a class="nav-link" href="contact-the-psp-secretariat-admin.php" style="color: grey;">Contact the PSP Secretariat</a>
					</li>
			    <form class="form-inline my-2 my-lg-0">

					<a href="index.php" class="btn btn-light my-2 my-sm-0">Log out</a>

			    </form>
				</ul>
				<form class="form-inline my-2 my-lg-0 d-none d-sm-block d-md-block" style="margin-left: auto;">
					<a href="logout.php" class="btnlogin btn btn-light mt-2">Log out</a>
				</form>
	</div>
</nav>


