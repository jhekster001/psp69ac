<div class="btnadmin btn-group-vertical pt-3" id="navbtn">
	<a style="background-color: #0d3b50 !important; width: 270px; height: 40px;" href="index-admin.php" class="btn btn-dark navbtn" id="home">Home</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="welcome-message-admin.php" class="btn btn-dark navbtn" id="welcome-message">Welcome Message</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="about-the-convention-admin.php" class="btn btn-dark navbtn" id="about-convention">About the Convention</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="convention-venue-admin.php" class="btn btn-dark navbtn" id="convention-venue">Convention Venue</a>

	<a style="background-color: #0d3b50 !important; height: 40px;"href="convention-house-rules-admin.php" class="btn btn-dark navbtn" id="house-rules">Convention House Rules</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="attendance-admin.php" class="btn btn-dark navbtn" id="attendance">Attendance</a>
	<a style="background-color: #0d3b50 !important; height: 40px;" href="announcements-admin.php" class="btn btn-dark navbtn" id="announcements">ANNOUNCEMENTS</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="organizing-committee-admin.php" class="btn btn-dark navbtn" id="organizing-committee">Organizing Committee</a>

	<a style="background-color: #0d3b50 !important; height: 40px;"href="awards-admin.php" class="btn btn-dark navbtn" id="awards">Awards</a>

	<a style="background-color: #0d3b50 !important; height: 40px;"href="fellowship-night-admin.php" class="btn btn-dark navbtn" id="fellowship">Fellowship Night</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="program-admin.php" class="btn btn-dark navbtn" id="program">Program</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="research-presentations-admin.php" class="btn btn-dark navbtn" id="research">RESEARCH PRESENTATIONS</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="keynote-speaker-admin.php" class="btn btn-dark navbtn" id="keynote-speaker">Keynote Speaker</a>

	<a style="background-color: #0d3b50 !important; height: 40px;"href="anatomic-pathology-speaker-admin.php" class="btn btn-dark navbtn" id="anatomic-speaker">Anatomic Pathology Speakers</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="clinical-pathology-speakers-admin.php" class="btn btn-dark navbtn" id="clinical-speaker">Clinical Pathology Speakers</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="registration-admin.php" class="btn btn-dark navbtn" id="registration">Registration</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="election-for-board-of-governors-admin.php" class="btn btn-dark navbtn" id="election-governors">Election for Board of Governors</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="congress-sponsors-admin.php" class="btn btn-dark navbtn" id="congress-sponsors">Congress Sponsors</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="the-psp-secretariat-admin.php" class="btn btn-dark navbtn" id="psp-secretariat">The PSP Secretariat</a>

	<a style="background-color: #0d3b50 !important; height: 40px;" href="contact-the-psp-secretariat-admin.php" class="btn btn-dark navbtn" id="contact-psp">Contact the PSP Secretariat</a>
</div>	