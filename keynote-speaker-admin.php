<?php
   include('session.php');
?>

<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/psp.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <title>PSP Admin</title>
  </head>
<body>

<?php
include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$opening_sql = "SELECT * FROM keynote_speaker WHERE type='opening'";
$closing_sql = "SELECT * FROM keynote_speaker WHERE type='closing'";
$result_opening = $conn->query($opening_sql);
$result_closing = $conn->query($closing_sql);

if ($result_opening->num_rows > 0) {
		$opening_details = $result_opening->fetch_assoc();
} else {
    echo "0 results";
}
if ($result_closing->num_rows > 0) {
		$closing_details = $result_closing->fetch_assoc();
} else {
    echo "0 results";
}
$conn->close();
?>

	<?php include('./components-admin/header-admin.php') ?>
		<div class="jumbotron bg-light mb-0 pb-0 mt-3">
			<div class="row">
				<div class="col-lg-2 d-none d-sm-block">					
					<?php include('./components-admin/sidebar-admin.php') ?>	
				</div>
					<!-- ************// PAGE//************ -->
					<div class="content col-lg-8 col-sm-12 mt-3 pt-3" id="home-content">
						<img src="img/keynote.jpg" id="banner" style="border-style: solid; border-color: grey;">
						<h4 class="title mt-3"><b>KEYNOTE SPEAKER</b></h4>
						<hr style="width: 100%; opacity: 0.2;">
						<br>
						<div class="container">
						<h5 style="color: green">Note: Updating image may have delay. Please refresh the page <kbd>ctrl+F5</kbd> to see results.</h5><br>
						<div class="row">
							<div class="col-5">	
								<div class="card">
									<img class="card-img-top" src="<?php echo $opening_details['file_path_dp'];?>" alt="Card image" style="width:100%">
									<div class="card-body">
										<h4 class="card-title"><?php echo $opening_details['fullname'] ?></h4>
										<p class="card-text"><?php echo $opening_details['position'] ?></p>
									</div>
								</div>
							</div> 
							<div class="col-7">
								<a href="keynote-speaker-edit.php" class="btn btn-success"><i class="fa fa-edit "></i> Edit</a>
								<h3><?php echo $opening_details['title'] ?></h3>
								<br>
								<h5><?php echo $opening_details['subtitle'] ?></h5>
								<br>
								<?php echo $opening_details['message'] ?>
							</div>
						</div>		
							<br><hr><br>
						</div>
						<div class="container">
							<div class="row">
								<div class="col-4">		
									<div class="card">
										<img class="card-img-top" src="<?php echo $closing_details['file_path_dp'];?>" alt="Card image" style="width:100%">
										<div class="card-body">
											<h4 class="card-title"><?php echo $closing_details['fullname'] ?></h4>
											<p class="card-text"><?php echo $closing_details['position'] ?></p>
										</div>
									</div>
								</div> 
								<div class="col-8">
									<h3><?php echo $closing_details['title'] ?></h3>
									<br>
									<h5><?php echo $closing_details['subtitle'] ?></h5>
									<br>
									<?php echo $closing_details['message'] ?>
									<br>
								</div>
							</div>		
							<br>
						</div>
					</div>
			</div>
		</div>
		<div class="p-5">
			<?php include('./components-admin/footer-admin.php') ?>
		</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>