<?php
   include('session.php');
include('./dbcon.php');

	if(isset($_POST['submit'])){
	//President
		$president_fullname = addslashes($_POST['president_fullname']);
		$president_position = addslashes($_POST['president_position']);
		$president_title = addslashes($_POST['president_title']);
		$president_subtitle = addslashes($_POST['president_subtitle']);
		$president_message = addslashes($_POST['president_message']);

		//Chairperson
		$chair_fullname = addslashes($_POST['chair_fullname']);
		$chair_position = addslashes($_POST['chair_position']);
		$chair_title = addslashes($_POST['chair_title']);
		$chair_subtitle = addslashes($_POST['chair_subtitle']);
		$chair_message = addslashes($_POST['chair_message']);


		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		$path = 'uploads/welcome/';
		if(!file_exists($path)){
			mkdir('uploads/welcome/', 0777, true);
		}
		$pres_ext = pathinfo($_FILES["president_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["president_dp"]["name"] != ''){
			$pres_target_file = $path . 'president-dp'.'.'.$pres_ext;
			move_uploaded_file($_FILES["president_dp"]["tmp_name"], $pres_target_file);		
		}else{
			$pres_select_sql = 'SELECT * from welcome_message WHERE type ="president"';
			$result_pres = $conn->query($pres_select_sql);
			$pres_details = $result_pres->fetch_assoc();
			$pres_target_file = $pres_details['file_path_dp'];		
		}

		$pres_sql = "UPDATE welcome_message SET 
				fullname='".$president_fullname."', 
				position='".$president_position."', 
				title='".$president_title."', 
				subtitle='".$president_subtitle."', 
				message='".$president_message."', 
				file_path_dp='".$pres_target_file."' 
			WHERE 
				type='president'";

		$chair_ext = pathinfo($_FILES["chair_dp"]["name"], PATHINFO_EXTENSION);		
		if($_FILES["chair_dp"]["name"] != ''){
			$chair_target_file = $path . 'chairperson-dp'.'.'.$chair_ext;
			move_uploaded_file($_FILES["chair_dp"]["tmp_name"], $chair_target_file);	
		}else{
			$chair_select_sql = 'SELECT * from welcome_message WHERE type ="chair"';
			$result_chair = $conn->query($chair_select_sql);
			$chair_details = $result_chair->fetch_assoc();
			$chair_target_file = $chair_details['file_path_dp'];

		}

		$chair_sql = "UPDATE welcome_message SET 
				fullname='".$chair_fullname."', 
				position='".$chair_position."', 
				title='".$chair_title."', 
				subtitle='".$chair_subtitle."', 
				message='".$chair_message."', 
				file_path_dp='".$chair_target_file."' 
			WHERE 
				type='chair'";		

		if ($conn->query($pres_sql) === TRUE && $conn->query($chair_sql)) {
			header("Location: welcome-message-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 