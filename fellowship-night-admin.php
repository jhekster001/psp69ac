<?php
   include('session.php');
?>

<?php
include('./dbcon.php');

// Create connection
$conn = new mysqli($servername, $username, $password, $dbname);
// Check connection
if ($conn->connect_error) {
    die("Connection failed: " . $conn->connect_error);
}

$sql = "SELECT * FROM fellowship";
$result = $conn->query($sql);

if ($result->num_rows > 0) {
		$fellowship_details = $result->fetch_assoc();
} else {
    echo "0 results";
}
// $conn->close();
?>

<!doctype html>

<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.2/css/all.css">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/psp.css">

    <!-- Custom CSS -->
    <link rel="stylesheet" href="css/styles.css">

    <title>PSP Admin</title>
  </head>
<body>

	<?php include('./components-admin/header-admin.php') ?>
		<div class="jumbotron bg-light mb-0 pb-0 mt-3">
			<div class="row">
				<div class="col-lg-2 d-none d-sm-block">					
					<?php include('./components-admin/sidebar-admin.php') ?>	
				</div>

				<!-- ************//CONTENT//************ -->
				<div class="content col-lg-8 col-sm-12 mt-3 pt-3 mb" id="home-content">
					<img src="img/fellowship.jpg" id="banner" style="border-style: solid; border-color: grey;">
					<h4 class="title mt-3"><b>FELLOWSHIP NIGHT</b></h4>

					<a href="#" class="btnView btn btn-primary" data-toggle="modal" data-target="#attendees"><i class="far fa-eye" ></i>&nbsp View Poll Result</a>


					<hr style="width: 100%; opacity: 0.2;">

					<a href="fellowship-night-edit.php" class="btn btn-success mb-3"><i class="fa fa-edit "></i> Edit</a>
					<h3><?php echo $fellowship_details['title']; ?></h3>
					<h4><?php echo $fellowship_details['subtitle']; ?></h4>
					<?php echo $fellowship_details['message']; ?><br>
					<img class="howto" src="<?php echo $fellowship_details['file_path_dp'];?>">
					<br>
				</div>
			</div>
		</div>

		<!-- VIEW POLL MODAL -->
		<div class="modal fade" id="attendees">
		  <div class="modal-dialog modal-dialog-scrollable" role="document">
		    <div class="modal-content">
		      <div class="modal-header">
		        <h5 class="modal-title"><b>Attendance Poll result</b></h5>
		        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
		          <span aria-hidden="true">&times;</span>
		        </button>
		      </div>
				<?php 
					// include('./dbcon.php');
					// $mysqli1 = new mysqli($servername, $username, $password, $dbname);

					if ($conn->connect_errno) {
					    printf("Connect failed: %s\n", $conn->connect_error);
					    exit();						 
					}

					$sql1 = "SELECT * FROM attendees ORDER BY id DESC";
						$result1 = $conn->query($sql1);

						$myObj1 = [];
						while ($row1 = $result1->fetch_assoc()) {		
						$myObj1[] = $row1;
						}
					?>
			      	<div class="modal-body">
				      	<table class="table">
							<thead class="thead-dark">
								<tr>
									<th scope="col">#</th>
						 			<th scope="col">Fullname</th>
						 			<th scope="col">Results</th>
								</tr>
							</thead>
						<?php 

						if ($result1){
							foreach($result1 as $row1)
						{
						?>
						<tbody>
							<tr>
								<td> <?php echo $row1['id']; ?></td>
								<td> <?php echo $row1['fullname']; ?></td>
								<td> <?php echo $row1['value']; ?></td>
					    	</tr>						    
						</tbody>
					<?php	
						}
					}					      
					?>

						</table>			
			      </div>
			      <div class="modal-footer">
			        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
			      </div>
			    </div>
			 </div>
		</div>

		<div class="p-5">
			<?php include('./components-admin/footer-admin.php') ?>
		</div>

    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js" integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

  </body>
</html>