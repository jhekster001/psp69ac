<?php
	include('session.php');
	include('./dbcon.php');

	if(isset($_POST['submit'])){
		$anatomic_id = addslashes($_POST['anatomic_id']);
		$anatomic_fullname = addslashes($_POST['fullname']);
		$anatomic_position = addslashes($_POST['position']);
		$anatomic_title = addslashes($_POST['title']);
		$anatomic_subtitle = addslashes($_POST['subtitle']);
		$anatomic_message = addslashes($_POST['message']);		

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		//Upload
		$path = 'uploads/anatomic/';
		if(!file_exists($path)){
			mkdir('uploads/anatomic/', 0777, true);
		}
		$anatomic_ext = pathinfo($_FILES["anatomic_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["anatomic_dp"]["name"] != ''){
			$anatomic_target_file = $path . 'anatomic-dp_'.$anatomic_id.'.'.$anatomic_ext;
			move_uploaded_file($_FILES["anatomic_dp"]["tmp_name"], $anatomic_target_file);		
		}else{
			$anatomic_select_sql = 'SELECT * from anatomic WHERE id="'.$anatomic_id.'" ';
			$result_anatomic = $conn->query($anatomic_select_sql);
			$anatomic_details = $result_anatomic->fetch_assoc();
			$anatomic_target_file = $anatomic_details['file_path_dp'];
		}

		$sql = "UPDATE anatomic SET 
				fullname='".$anatomic_fullname."', 
				position='".$anatomic_position."',
				title='".$anatomic_title."', 
				subtitle='".$anatomic_subtitle."', 
				message='".$anatomic_message."', 
				file_path_dp='".$anatomic_target_file."'
				WHERE id='".$anatomic_id."'";

		if ($conn->query($sql) === TRUE) {
		    header("Location: anatomic-pathology-speaker-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 
