<?php
   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$committee_id = addslashes($_POST['committee_id']);
		$committee_fullname = addslashes($_POST['fullname']);
		$committee_position = addslashes($_POST['position']);

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		//Upload
		$path = 'uploads/committee/';
		if(!file_exists($path)){
			mkdir('uploads/committee/', 0777, true);
		}
		$committee_ext = pathinfo($_FILES["committee_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["committee_dp"]["name"] != ''){
			$committee_target_file = $path . 'committee-dp_'.$committee_id.'.'.$committee_ext;
			move_uploaded_file($_FILES["committee_dp"]["tmp_name"], $committee_target_file);		
		}else{
			$committee_select_sql = 'SELECT * from committee WHERE id="'.$committee_id.'" ';
			$result_committee = $conn->query($committee_select_sql);
			$committee_details = $result_committee->fetch_assoc();
			$committee_target_file = $committee_details['file_path_dp'];
		}

		$sql = "UPDATE committee SET 
				fullname='".$committee_fullname."', 
				position='".$committee_position."',
				file_path_dp='".$committee_target_file."'
				WHERE id='".$committee_id."'";

		if ($conn->query($sql) === TRUE) {
			header("Location: organizing-committee-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 