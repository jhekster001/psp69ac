<?php

   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$awards_fullname = addslashes($_POST['fullname']);
		$awards_position = addslashes($_POST['position']);
		$awards_title = addslashes($_POST['title']);
		$awards_subtitle = addslashes($_POST['subtitle']);
		$awards_message = addslashes($_POST['message']);		

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		$sql = "INSERT INTO awards SET 
				fullname='".$awards_fullname."', 
				position='".$awards_position."',
				title='".$awards_title."', 
				subtitle='".$awards_subtitle."', 
				message='".$awards_message."', 
				file_path_dp=''";

		if ($conn->query($sql) === TRUE) {
		    $last_id = $conn->insert_id;
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		//Upload
		$path = 'uploads/awards/';
		if(!file_exists($path)){
			mkdir('uploads/awards/', 0777, true);
		}
		$awards_ext = pathinfo($_FILES["awards_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["awards_dp"]["name"] != ''){
			$awards_target_file = $path . 'awards-dp_'.$last_id.'.'.$awards_ext;
			move_uploaded_file($_FILES["awards_dp"]["tmp_name"], $awards_target_file);		
		}else{
			$awards_target_file = '';
		}		
		$update_sql = "UPDATE awards SET 
						file_path_dp='".$awards_target_file."' 
						WHERE id='".$last_id."'";
		if ($conn->query($update_sql) === TRUE) {
			header("Location: awards-admin.php");
		} else {
		    echo "Error: " . $update_sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>


 
