// LOGIN SCRIPT
$(document).ready(function() {
	$('#loginForm').submit(function(e){
		e.preventDefault();
		$('#loader').show();
		$('#invalid').hide();
		let postData = {
			submit: 'submit',
			username: $('#username').val(),
			password: $('#password').val(),
		}

		$.ajax({
			type: 'post',
			url: 'login.php',
			data: postData,
			success: function(response){
				if(response.trim() === 'success'){
					window.location.href="index-admin.php";
				}else{
					$('#loader').hide();
					$('#invalid').show();
				}
			}
		})
	});
});

// POLL //

$(document).ready(function() {

	$.getJSON( "/getpolls.php", function( data ) {
	  var links = data;
	  	$.each(data, function( key, value ) {
	  		var width = value.count >= 1000 ? '1150' : (value.count*1+350)
			$('#links').append(`
				<div  style="cursor:pointer; width:`+width +`px;" id="click" data-selected="`+value.value+`" data-count="`+value.count+`">
					<span>`+value.count+`</span>
					&nbsp<i class="`+value.fontawesome+`"></i>
					<b>&nbsp`+value.text+`</b>
				</div>
			`)
		})

		$('div#click').each(function(index){
			$(this).click(function(){
				let confirmation = prompt('Please enter your fullname')
				if(confirmation != '' && confirmation != null){
					$.post( "/savePoll.php", { 
						value: $(this).data('selected'), 
						name: confirmation,
						submit: 'submit'})
					  .done(function( data ) {
					  	location.reload();
					  	// localStorage.setItem("attendance", true);
					});
				}
			})
		})		
	});

	// if(!localStorage.getItem("attendance")){


	// }else{
	// 	$.getJSON( "/getpolls.php", function( data ) {
	// 	  console.log(data)
	// 	  var links = data;
	// 	  	$.each(data, function( key, value ) {
	// 	  		var width = value.count >= 1000 ? '1150' : (value.count*1+350)
	// 			$('#links').append(`
	// 				<div  style="cursor:not-allowed; width:`+width +`px;" id="click" data-selected="`+value.value+`" data-count="`+value.count+`">
	// 					<span>`+value.count+`</span>
	// 					&nbsp<i class="`+value.fontawesome+`"></i>
	// 					<b>&nbsp `+value.text+`</b>
	// 				</div>
	// 			`)
	// 		})

	// 	});		
	// }

    $("#poll #links #click").click(function() {
        $(this).parent().animate({
           width: '+=.5px'
        }, 500);

        $(this).prev().html(parseInt($(this).prev().html()) + 1);
        return false;
    });
});


