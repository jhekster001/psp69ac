<?php
   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$attendance_title = addslashes($_POST['title']);
		$attendance_subtitle = addslashes($_POST['subtitle']);

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		 //Upload
		$path = 'uploads/attendance/';
		if(!file_exists($path)){
			mkdir('uploads/attendance/', 0777, true);
		}
		$attendance_ext = pathinfo($_FILES["attendance_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["attendance_dp"]["name"] != ''){
			$attendance_target_file = $path . 'attendance'.'.'.$attendance_ext;
			move_uploaded_file($_FILES["attendance_dp"]["tmp_name"], $attendance_target_file);		
		}else{
			$attendance_select_sql = 'SELECT * from attendance';
			$result_attendance = $conn->query($attendance_select_sql);
			$attendance_details = $result_attendance->fetch_assoc();
			$attendance_target_file = $attendance_details['file_path_dp'];
		}

		$sql = "UPDATE attendance SET 
				title='".$attendance_title."', 
				subtitle='".$attendance_subtitle."',  
				file_path_dp='".$attendance_target_file."'";

		if ($conn->query($sql) === TRUE) {
			header("Location: attendance-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 