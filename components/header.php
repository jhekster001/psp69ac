<!doctype html>
<html lang="en">
<link rel="shortcut icon" href="./img/psp-favicon2.png" />

  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

    <!-- Bootstrap CSS -->
    <link rel="stylesheet" href="css/psp.css">

    <!-- Custom CSS -->.
    <link rel="stylesheet" href="css/poll.css">
    <link rel="stylesheet" href="css/styles.css">
    <link rel="stylesheet" type="text/css" href="css/main.css">
    <link rel="stylesheet" type="text/css" href="css/card.css">
    <link rel="stylesheet" href="css/fontawesome/all.css">
    <title>PSP 69th Annual Convention</title>
  </head>
<body>
	<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="topnav">
		<img class="imgheader" src="img/header_02.jpg">
		<a class="navbar-brand" href="index.php"><img class="logo" src="img/logo.png" id="logo"></a>
		<div class="container col-10">	
			<button class="navbar-toggler bg-dark d-xl-none d-lg-none d-md-none" type="button" data-toggle="collapse" data-target="#navbarColor02" aria-controls="navbarColor02" aria-expanded="false" aria-label="Toggle navigation" style="margin-left: 240px;">
			<span class="navbar-toggler-icon"></span>
			</button>
				<div class="collapse navbar-collapse" id="navbarColor02">
					<ul class="navbar-nav mr-auto d-xl-none d-lg-none d-md-none">
				    	<li class="nav-item active">
							<a class="nav-link" href="index.php">Home</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="welcome-message.php">Welcome Message</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="about-the-convention.php">About the Convention</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="convention-venue.php">Convention Venue</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="research-presentations.php">Research Presentations</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="convention-house-rules.php">Convention House Rules</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="attendance.php">Attendance</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="organizing-committee.php">Organizing Committee</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="keynote-speaker.php">Keynote Speaker</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="anatomic-pathology-speaker.php">Anatomic Pathology Speakers</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="clinical-pathology-speakers.php">Clinical Pathology Speakers</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="awards.php">Awards</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="fellowship-night.php">Fellowship Night</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="program.php">Program</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="registration.php">Registration</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="election-for-board-of-governors.php">Election for Board of Govervors</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="congress-sponsors.php">Congress Sponsors</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="the-psp-secretariat.php">The PSP Secretariat</a>
						</li>
						<li class="nav-item">
						<a class="nav-link" href="contact-the-psp-secretariat.php">Contact the PSP Secretariat</a>
						</li>
				    <form class="form-inline my-2 my-lg-0">
						<a href="javascript:void(0)" class="btnlogin btn btn-outline-light my-2 my-sm-0" data-toggle="modal" data-target="#login" style="width: 90px;">Sign In</a>
				    </form>
					</ul>

					<!-- TOP NAV -->
					<form class="form-inline my-2 my-lg-0 d-none d-sm-block d-md-block" style="margin-left: auto;">

						<a href="javascript:void(0)" class="btntop" id="modalVideoBtn" data-toggle="modal" data-target="#video" title=" PSP Pre-Registration Video"><i class="fas fa-play-circle" style="font-size: 20px; text-shadow: 2px 2px black;"></i></a>	

						<a href="announcements.php" class="btntop" title="Announcements"><i class="fas fa-bullhorn" style="font-size: 20px; text-shadow: 2px 2px black;"></i></a>
						<a href="https://www.facebook.com/Philippine-Society-of-Pathologists-232421263467403/" target="_blank" class="btntop" title="PSP Facebook Page"><i class="fab fa-facebook" style="font-size: 20px; text-shadow: 2px 2px black;"></i></a>

						<a class="btntop" href="index.php" style="text-shadow: 2px 2px black;">HOME</a>
						<a class="btntop" href="registration.php" style="text-shadow: 2px 2px black;">REGISTRATION</a>
						<a class="btntop" href="contact-the-psp-secretariat.php" style="text-shadow: 2px 2px black;">CONTACT US</a>							
						<a href="javascript:void(0)" class="btnlogin btn btn-outline-light my-2 my-sm-0" data-toggle="modal" data-target="#login" style="width: 90px;">Sign In</a>

						<!-- POLL -->
						<!-- <div id="poll">
							<p style="text-shadow: 2px 2px black;"><i class="fas fa-vote-yea"></i>&nbsp PLEASE TELL US YOUR PLANS.</p>
							<hr style="border-color: white;">
								<div id="links">
							      
								</div>
						</div> -->
					</form>
				</div>
			</div>
		</div>
	</nav>

	<!-- ****//MODAL//**** -->
	<div class="modal fade" id="login">
		<div class="modal-dialog" role="document">
			<div class="modal-content">
				<div class="modal-header">
					<h5 class="modal-title w-100 font-weight-bold">SIGN IN</h5>
					<button type="button" class="close" data-dismiss="modal" aria-label="close">
						<span aria-hidden="true">&times;</span>
					</button>
				</div>

				<form method="POST" id="loginForm">
					<div class="modal-body">
						<div class="form-group">
							<label for="username">Username</label>
							<input type="text" name="username" class="form-control" id="username" placeholder="Enter Username" autocomplete="off">
						</div>
						<div class="form-group">
							<label for="password" id="pass">Password</label>
							<input type="password" name="password" class="form-control" id="password" placeholder="Enter Password">
						</div>
						<p style="display: none;" id="loader"><i class="fa fa-spinner fa-spin" ></i> Please wait</p>
						<h6 style="color: red; display: none" id="invalid">*Username/Password is Invalid!</h6>
					</div>

					<div class="modal-footer">
						<button type="submit" name="submit" class="btn btn-success" id="btnlogin">Sign-in</button>
						<button type="button" class="btn btn-secondary" data-dismiss="modal" id="btnclose">Close</button>
					</div>
				</form>
			</div>
		</div>
	</div>

	<!-- ****//MODAL-VIDEO//**** -->
	<div class="modal fade" tabindex="-1" role="dialog" id="video">
	  <div class="modal-dialog modal-lg" role="document">
	    <div class="modal-content">
	      <div class="modal-header">
	        <h5 class="modal-title">PSP Pre-Registration</h5>
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	          <span aria-hidden="true">&times;</span>
	        </button>
	      </div>
	      <div class="modal-body embed-responsive embed-responsive-16by9">
	      	<!-- <embed src="img/intro.mp4" id="myVideo"></embed> -->
	      	<iframe src="" id="myVideo"></iframe>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-secondary" data-dismiss="modal" id="closeModal">Close</button>
	      </div>
	    </div>
	  </div>
	</div>