<?php include('./components/header.php') ?>

<div class="jumbotron mb-0 mt-3" id="bg1" style="padding-top: 220px;">
	<div class="row">
		<div class="col-lg-3 col-md-3 d-none d-sm-block pt-3">					
			<?php include('./components/sidebar.php') ?>	
		</div>

		<!-- ************//CONTENT//************ -->
		<div class="col-lg-8 col-md-8 col-sm-12 " id="home-content">
			<div class="content jumbotron mt-3 pt-4 pb-5 rounded-0" style="width: 1240px;">			
				
				<img src="img/postponed.jpg" id="banner" style="border-style: solid; border-color: grey;">					
				<p></p>
				<p></p>
				<h4><b>PHILIPPINE SOCIETY OF PATHOLOGIST. INC - 1950</b></h4>
				<p>The organizational meeting was held at the Aristocrat Restaurant on Roxas Boulevard on the night of April 11, 1950. The first set of officers was elected namely: President, Dr. Liborio Gomez; Vice-President, Dr. Juan Z. Sta. Cruz; Secreatry-Treasurer, Dr. Benjamin A, Barrera; Governors: Drs. Onofre Garcia, Walfredo De Leon, Manuel D. Penas, and Benjamin A. Barrera.</p>

				<p>The PSP Board of Pathology was created in the year 1966-1967 to administer the examinations and grants certificates of qualification for the practice of pathology after satisfactory demonstration as Regular Member and eventual evaluation of Fellow in the Society.</p>

				<p>On June 18, 1966 the Clinical Laboratory Law (RA 4688) was passed by the 6th Congress of the Philippines with valuable contributions extended by the Society in defining the role of the profession and scope of practice of pathology in the country. Subsequently the implementing rules governing the establishment, registration, operation and maintenance of clinical laboratories were approved and signed by then Secretary of Health Paulino J. Garcia. The Society was also admitted as a specialty affiliate of the Philippine Medical Association.</p>

				<p>In the following years, the need for improving quality patient care with assurance of quality laboratory output became the specific thrust of the Society with the cooperation of the Bureau of Research and Laboratories.</p>

				<p>The Society undertook series of seminars, workshops and symposia in collaboration with some national specialty, international organizations such as the World Health Organization and government agencies particularly the Ministry of Health. These activities are now the mainstay of the current thrust to upgrade both manpower and institutional developments of the country.</p>

				<p>The ground breaking and cornerstone laying on the side of the future PSP builing was held on February 20, 1986. The PSP also became member of the Asia Pacific Association of Society of Pathologists.</p>

				<p>The turn of the century was characterized by rapid advances in Medicine, computer technology and communication. The new millennium poses new challenges. As the Society celebrates its 50th year of existence, its commitment to excellence in health service, training and research remains unabated bringing the practice of pathology to a renewed and grater height. It could be stated that the practice of pathology in the Philippines is at par with those in other countries and the Filipino pathologists is an important partner of other doctors in the management of patients.
			</div>
		</div>
	</div>
</div>

<div>
<?php include('./components/footer.php') ?>
</div>