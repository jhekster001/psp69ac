<?php

   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
	//President
		$sponsors_title = addslashes($_POST['title']);
		$sponsors_subtitle = addslashes($_POST['subtitle']);

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		//Upload
		$path = 'uploads/sponsors/';
		if(!file_exists($path)){
			mkdir('uploads/sponsors/', 0777, true);
		}
		$sponsors_ext = pathinfo($_FILES["sponsors_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["sponsors_dp"]["name"] != ''){
			$sponsors_target_file = $path . 'sponsors'.'.'.$sponsors_ext;
			move_uploaded_file($_FILES["sponsors_dp"]["tmp_name"], $sponsors_target_file);		
		}else{
			$sponsors_select_sql = 'SELECT * from sponsors';
			$result_sponsors = $conn->query($sponsors_select_sql);
			$sponsors_details = $result_sponsors->fetch_assoc();
			$sponsors_target_file = $sponsors_details['file_path_dp'];
		}

		$sql = "UPDATE sponsors SET
				title='".$sponsors_title."', 
				subtitle='".$sponsors_subtitle."', 
				file_path_dp='".$sponsors_target_file."'";

		if ($conn->query($sql) === TRUE) {
			header("Location: congress-sponsors-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 