<?php

   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$awards_id = addslashes($_POST['awards_id']);
		$awards_fullname = addslashes($_POST['fullname']);
		$awards_position = addslashes($_POST['position']);
		$awards_title = addslashes($_POST['title']);
		$awards_subtitle = addslashes($_POST['subtitle']);
		$awards_message = addslashes($_POST['message']);		

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		//Upload
		$path = 'uploads/awards/';
		if(!file_exists($path)){
			mkdir('uploads/awards/', 0777, true);
		}
		$awards_ext = pathinfo($_FILES["awards_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["awards_dp"]["name"] != ''){
			$awards_target_file = $path . 'awards-dp_'.$awards_id.'.'.$awards_ext;
			move_uploaded_file($_FILES["awards_dp"]["tmp_name"], $awards_target_file);		
		}else{
			$awards_select_sql = 'SELECT * from awards WHERE id="'.$awards_id.'" ';
			$result_awards = $conn->query($awards_select_sql);
			$awards_details = $result_awards->fetch_assoc();
			$awards_target_file = $awards_details['file_path_dp'];
		}

		$sql = "UPDATE awards SET 
				fullname='".$awards_fullname."', 
				position='".$awards_position."',
				title='".$awards_title."', 
				subtitle='".$awards_subtitle."', 
				message='".$awards_message."', 
				file_path_dp='".$awards_target_file."'
				WHERE id='".$awards_id."'";

		if ($conn->query($sql) === TRUE) {
			header("Location: awards-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

 
