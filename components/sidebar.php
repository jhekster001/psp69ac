<ul class="ca-menu">
    <a href="index.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-home"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Home</h2>
                <h3 class="ca-sub">Philippine Society of Pathologist</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="welcome-message.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-comments"></i></span>
            <div class="ca-content ">
                <h2 class="ca-main">Welcome Messages</h2>
                <h3 class="ca-sub">Messages from President and Chairperson</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="about-the-convention.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-info-circle"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">About the Convention</h2>
                <h3 class="ca-sub">The PSP Annual Convention</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="convention-venue.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-map-marker-alt"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Convention Venue</h2>
                <h3 class="ca-sub">Venue for PSP Annual Convention</h3>
            </div>
        </label>
    </li>
    </a>


    <a href="convention-house-rules.php">
      <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="far fa-list-alt"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Convention House Rules</h2>
                <h3 class="ca-sub">Rules and Guidelines</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="attendance.php">
      <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-clipboard-check"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Attendance</h2>
                <h3 class="ca-sub">The How to Guide</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="organizing-committee.php">
      <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-sitemap"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Organizing Committee</h2>
                <h3 class="ca-sub">The PSP Committee</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="awards.php">
     <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-trophy"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Awards</h2>
                <h3 class="ca-sub">The Committee on Awards</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="fellowship-night.php">
     <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="far fa-bookmark"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Fellowship Night</h2>
                <h3 class="ca-sub">The Greatest PSP Musical and Talent Show</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="program.php">
     <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-chalkboard-teacher"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Program</h2>
                <h3 class="ca-sub">The Time and Date Schedule</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="research-presentations.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-search"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Research Presentations</h2>
                <h3 class="ca-sub">PSP Research Presentations</h3>
            </div>
        </label>
    </li>
    </a>
    
    <a href="keynote-speaker.php">
     <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-book-reader"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Keynote Speaker</h2>
                <h3 class="ca-sub">Opening and Closing Remarks</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="anatomic-pathology-speaker.php">
     <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-atom"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Anatomic Pathology Speakers</h2>
                <h3 class="ca-sub">Anatomic Speakers and Abstract</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="clinical-pathology-speakers.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-user-md"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Clinical Pathology Speakers</h2>
                <h3 class="ca-sub">Clinical Speakers and Abstract</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="registration.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-clipboard-list"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Registration</h2>
                <h3 class="ca-sub">Registration Guidelines</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="election-for-board-of-governors.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-vote-yea"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Election for Board of Governors</h2>
                <h3 class="ca-sub">Board of Governors Guidelines</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="congress-sponsors.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="far fa-handshake"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Congress Sponsors</h2>
                <h3 class="ca-sub">The PSP Sponsors</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="the-psp-secretariat.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-users"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">The PSP Secretariat</h2>
                <h3 class="ca-sub">Responsible for the Events</h3>
            </div>
        </label>
    </li>
    </a>

    <a href="contact-the-psp-secretariat.php">
    <li>
        <label class="mlabel">
            <span class="ca-icon"><i class="fas fa-phone-volume"></i></span>
            <div class="ca-content">
                <h2 class="ca-main">Contact Us</h2>
                <h3 class="ca-sub">For Questions and Suggestions</h3>
            </div>
        </label>
    </li>
    </a>
</ul>