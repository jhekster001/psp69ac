<?php
   include('session.php');

include('./dbcon.php');

	if(isset($_POST['submit'])){
		$program_title = addslashes($_POST['title']);
		$program_subtitle = addslashes($_POST['subtitle']);	

		// Create connection
		$conn = new mysqli($servername, $username, $password, $dbname);
		// Check connection
		if ($conn->connect_error) {
		    die("Connection failed: " . $conn->connect_error);
		}

		//Upload
		$path = 'uploads/program/';
		if(!file_exists($path)){
			mkdir('uploads/program/', 0777, true);
		}
		$program_ext = pathinfo($_FILES["program_dp"]["name"], PATHINFO_EXTENSION);		

		if($_FILES["program_dp"]["name"] != ''){
			$program_target_file = $path . 'program'.'.'.$program_ext;
			move_uploaded_file($_FILES["program_dp"]["tmp_name"], $program_target_file);		
		}else{
			$program_select_sql = 'SELECT * from program';
			$result_program = $conn->query($program_select_sql);
			$program_details = $result_program->fetch_assoc();
			$program_target_file = $program_details['file_path_dp'];
		}

		$sql = "UPDATE program SET 
				title='".$program_title."', 
				subtitle='".$program_subtitle."',
				file_path_dp='".$program_target_file."'";

		if ($conn->query($sql) === TRUE) {
			header("Location: program-admin.php");
		} else {
		    echo "Error: " . $sql . "<br>" . $conn->error;
		}

		$conn->close();	
	}

	
 ?>

